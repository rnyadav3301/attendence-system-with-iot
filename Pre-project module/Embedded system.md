#  embedded system

An embedded system is a combination of computer hardware and software designed for a specific function or functions within a larger system. The systems can be programmable or with fixed functionality. Industrial machines, consumer electronics, agricultural and process industry devices, automobiles, medical equipment, cameras, household appliances, airplanes, vending machines and toys, as well as mobile devices, are possible locations for an embedded system.



While embedded systems are computing systems, they can range from having no user interface (UI) -- for example, on devices in which the system is designed to perform a single task -- to complex graphical user interfaces (GUIs), such as in mobile devices. User interfaces can include buttons, LEDs and touchscreen sensing. Some systems use remote user interfaces as well.

**Characteristics of embedded systems**

The main characteristic of embedded systems is that they are task specific. They perform a single task within a larger system. For example, a mobile phone is not an embedded system, it is a combination of embedded systems that together allow it to perform a variety of general-purpose tasks. The embedded systems within it perform specialized functions. For example, the GUI performs the singular function of allowing the user to interface with the device. In short, they are programmable computers, but designed for specific purposes, not general ones.

Additionally, embedded systems can include the following characterstics

- comprised of hardware, software and firmware;
- embedded in a larger system to perform a specific function as they are built for specialized tasks within the system, not various tasks;
- either microprocessor-based or microcontroller-based -- both are integrated circuits that give the system compute power;
- often used for sensing and real-time computing in internet of things (IoT) devices -- devices that are internet-connected and do not require a user to operate;
- vary in complexity and in function, which affects the type of software, firmware and hardware they use
- often required to perform their function under a time constraint to keep the larger system functioning properly.

# Embedded systems vary in complexity, but generally consist of three main elements:

- **Hardware**. The hardware of embedded systems is based around microprocessors and microcontrollers. Microprocessors are very similar to microcontrollers, and generally refer to a CPU that is integrated with other basic computing components such as memory chips and digital signal processors (DSP). Microcontrollers have those components built into one chip.
- **Software**. Software for embedded systems can vary in complexity. However, industrial-grade microcontrollers and embedded IoT systems generally run very simple software that requires little memory.
- **Firmware**. Embedded firmware is usually used in more complex embedded systems to connect the software to the hardware. Firmware is the software that interfaces directly with the hardware. A simpler system may just have software directly in the chip, but more complicated systems need firmware under more complex software applications and operating systems.


#  Embedded system hardware

Embedded system hardware can be microprocessor- or microcontroller-based. In either case, an integrated circuit is at the heart of the product that is generally designed to carry out real-time computing. Microprocessors are visually indistinguishable from microcontrollers. However, the microprocessor only implements a central processing unit (CPU) and, thus, requires the addition of other components such as memory chips. Conversely, microcontrollers are designed as self-contained systems.

#  Embedded system software

A typical industrial microcontroller is unsophisticated compared to the typical enterprise desktop computer and generally depends on a simpler, less-memory-intensive program environment. The simplest devices run on bare metal and are programmed directly using the chip CPU's machine code language.

Often, embedded systems use operating systems or language platforms tailored to embedded use, particularly where real-time operating environments must be served. At higher levels of chip capability, such as those found in SoCs, designers have increasingly decided the systems are generally fast enough and the tasks tolerant of slight variations in reaction time that near-real-time approaches are suitable. In these instances, stripped-down versions of the Linux operating system are commonly deployed, although other operating systems have been pared down to run on embedded systems, including Embedded Java and Windows IoT (formerly Windows Embedded).

Generally, storage of programs and operating systems on embedded devices make use of either flash or rewritable flash memory.

#  Embedded firmware

 is specific software written into the memory of a device that serves the purpose of ROM but can be updated more easily. Firmware can be stored in non-volatile memory devices, including ROM,

[RasPi Camera](https://projects.raspberrypi.org/en/projects/getting-started-with-picamera)

%% ==========================================Your region===============================================================%%